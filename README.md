# Create new account for existing customers
### Development

Please install the `Lombok` plugin in your IDE for it to recognize lombok annotations like `@Getter` `@Setter` etc...

# Build Application

 Execute below docker build file so that it will build docker image for the applications.
```shell script
accounts-service/docker_build.sh
transactions-service/docker_build.sh
```             
    
Now run the following commands to start the application
```shell script
docker-compose up -d
```

# Accessing the Application

Created a simple standalone `HTML` page with combination `JQuery` to perfom below operations
- Create Account
- Initiate Transaction
- Check Transaction

Open the `dashboard.html` in a web browser to use the application
